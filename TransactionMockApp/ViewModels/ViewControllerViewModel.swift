//
//  ViewControllerModel.swift
//  TransactionMockApp
//
//  Created by Raphael Pedrini Velasqua on 02/10/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class ViewControllerViewModel {

    let service = TransactionService()

    var transactions: [Transaction]?

    var isLoading = false

    var controller: ViewController!

    init(controller: ViewController) {
        self.controller = controller
    }

    func loadTransactions() {
        controller.startLoading()
        service.getTransactionList(doneLoadingTransactions)
    }

    func doneLoadingTransactions(result: [Transaction]?) {
        controller.stopLoading()

        guard let transactions = result else {
            return
        }

        self.transactions = transactions
        self.controller.loadTableView()
    }
}
