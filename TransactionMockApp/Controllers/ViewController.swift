//
//  ViewController.swift
//  TransactionMockApp
//
//  Created by Raphael Pedrini Velasqua on 27/09/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var viewModel: ViewControllerViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = ViewControllerViewModel(controller: self)
        viewModel.loadTransactions()

        setupTableView()
    }

    func setupTableView() {
        tableView.isHidden = true

        tableView.register(UINib(nibName: String(describing: TransactionCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TransactionCell.self))

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80

        tableView.dataSource = self
    }

    func startLoading() {
        tableView.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    func stopLoading() {
        tableView.isHidden = false
        activityIndicator.isHidden = true
    }

    func loadTableView() {
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let transactionsCount = viewModel.transactions?.count {
            return transactionsCount
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TransactionCell.self)) as? TransactionCell else {
            return UITableViewCell()
        }

        cell.loadData(transaction: viewModel.transactions![indexPath.row])

        return cell
    }
}

