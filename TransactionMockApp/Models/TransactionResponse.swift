//
//  TransactionResponse.swift
//  TransactionMockApp
//
//  Created by Raphael Pedrini Velasqua on 02/10/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

struct TransactionResponse: Decodable {

    var data: [Transaction]

    enum TransactionsKey: CodingKey {
        case data
    }
}

class Transaction: Decodable {
    var id: String
    var date: String
    var description: String
    var categoryId: Int
    var currency: String
    var amount: Amount
    var product: Product

    private enum CodingKeys: String, CodingKey {
        case id
        case date
        case description
        case categoryId = "category_id"
        case currency
        case amount
        case product
    }
}

struct Amount: Decodable {
    var value: Double
    var currencyIso: String

    private enum CodingKeys: String, CodingKey {
        case value
        case currencyIso = "currency_iso"
    }
}

struct Product: Decodable {
    var id: Int
    var title: String
    var iconUrl: String

    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case iconUrl = "icon"
    }
}
