
//
//  TransactionService.swift
//  TransactionMockApp
//
//  Created by Raphael Pedrini Velasqua on 27/09/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class TransactionService {

    func getTransactionList(_ completition: @escaping (_ result: [Transaction]?) -> Void) {

        let url = URL(string: "https://www.mocky.io/v2/5b33bdb43200008f0ad1e256")!

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completition(nil)
                }
                return
            }

            do {
                let decoder = JSONDecoder()
                let transactions = try decoder.decode(TransactionResponse.self, from: data)
                DispatchQueue.main.async {
                    completition(transactions.data)
                }
            } catch _ {
                DispatchQueue.main.async {
                    completition(nil)
                }
            }
        }

        task.resume()
    }
}
