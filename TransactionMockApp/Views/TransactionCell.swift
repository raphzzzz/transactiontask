//
//  TransactionCell.swift
//  TransactionMockApp
//
//  Created by Raphael Pedrini Velasqua on 02/10/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var transactionIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func loadData(transaction: Transaction) {
        descriptionLabel.text = transaction.description
        dateLabel.text = transaction.date
        currencyLabel.text = "£\(transaction.amount.value)"

        let url = URL(string: transaction.product.iconUrl)
        let data = try? Data(contentsOf: url!)
        transactionIcon.image = UIImage(data: data!)
    }
}
