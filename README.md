# Bud Transaction Task 
Developed using Swift 4.2 and Xcode 10

## To run the project 
- Requirements: Xcode 10, Swift 4.2
- Clone the repository
- Open it on Xcode
- Build

## What would I do next
- Image cache (I would recommend a Pod for this (like SDWebImage). saves a huge amount of time.)
- Error handling
- More tests
- Richer UI with pull to refresh
