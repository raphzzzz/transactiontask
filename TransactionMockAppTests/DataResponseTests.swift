//
//  DataResponseTests.swift
//  TransactionMockAppTests
//
//  Created by Raphael Pedrini Velasqua on 02/10/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import XCTest

@testable import TransactionMockApp

class DataResponseTests: XCTestCase {

    var bundle: Bundle!

    var path: URL!

    var transactions: [Transaction] = []

    override func setUp() {
        bundle = Bundle(for: DataResponseTests.self)

        path = bundle.url(forResource: "transactions_mock", withExtension: "json")
    }

    func testInit_TransactionsNotNil() {
        do {
            let data = try Data(contentsOf: path)
            let decoder = JSONDecoder()
            let transactions = try decoder.decode(TransactionResponse.self, from: data)
            XCTAssertNotNil(transactions)
        } catch _ {
            XCTAssertNil(transactions)
        }
    }
}
